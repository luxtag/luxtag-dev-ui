# LuxTag UIUX Developer Initiation

## Goal

Take some time to explore the learning resources listed below. You can get started on the given task by forking this repository under your own Bitbucket account. This repository is a cut-down version of the LuxTag user application. Go to https://barsalux.surge.sh/ to see the full application.

Your task is to improve the look and feel of the home, sign-up and confirmation views. You may use a different CSS element and grid framework (Bootstrap, Bulma etc.). Your work will primarily be within the `<template></template>` tags. Make note of any changes you would like to implement, if you weren't able to do so within the testing period.

## Learning Resources
1. Introduction to blockchain technology: https://youtu.be/bBC-nXj3Ng4
2. VueJS 2 Tutorial playlist: https://www.youtube.com/playlist?list=PL4cUxeGkcC9gQcYgjhBoeQH7wiAyZNrYa
3. NEM-SDK documentation: https://github.com/QuantumMechanics/NEM-sdk
4. NIS API documentation: https://bob.nem.ninja/docs/

## UIUX Notes
This project uses components from [Element-UI](http://element.eleme.io/#/en-US/resource).

## Build Setup

Requires a recent installation of NodeJS and npm.

``` bash
# install dependencies
npm install

# serve with hot reload at localhost:8080. (ignore the compiler warnings)
npm run dev

# build for production with minification
npm run build

# build for production and view the bundle analyzer report
npm run build --report

# run unit tests
npm run unit

# run all tests
npm test
```

For detailed explanation on how things work, checkout the [guide](http://vuejs-templates.github.io/webpack/) and [docs for vue-loader](http://vuejs.github.io/vue-loader).
