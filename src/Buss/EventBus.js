// examples of how to use the even bus can be found
// here https://alligator.io/vuejs/global-event-bus/
import Vue from 'vue';

const EventBus = new Vue();
export default EventBus;
