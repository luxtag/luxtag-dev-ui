import Vue from 'vue';
import Router from 'vue-router';
import Signup from '@/components/Signup';
// code splitting in action for routs
// this will layzily load the rout by issuing a promise to webpack
const Home = () => import('@/components/Home');
const Confirmation = () => import('@/components/Confirmation');

Vue.use(Router);

const router = new Router({
  routes: [
    {
      path: '/',
      name: 'Home',
      component: Home,
    },
    {
      // Sign UP will be rendered inside User's <router-view>
      path: '/sign-up',
      name: 'Sign-up',
      component: Signup,
    },
    {
      // Sign UP will be rendered inside User's <router-view>
      path: '/confirmation',
      name: 'Confirmation',
      component: Confirmation,
    },
  ],
});

export default router;
