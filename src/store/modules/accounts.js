import * as types from '../mutation-types';
import store from '../index';
import EventBus from '../../Buss/EventBus';

// initial state
const state = {
  wallets: [],
};

// getters
const getters = {
  getAccounts: state => state.wallets,
};

// mutations
const mutations = {
  [types.STORE_ACCOUNT](state, wallet) {
    state.wallets.push(wallet);
  },
  [types.STORE_ACCOUNTS](state, accounts) {
    state.wallets = accounts;
  },
};

// actions
const actions = {
  storeAccount(context, wallet) {
    context.commit('STORE_ACCOUNT', wallet);
  },
  purge(context, toPurge) {
    let adjust = true;
    // we check if we are loged in then
    if (!store.getters.getAuth.login) {
      adjust = false;
    }
    /* eslint no-var: "off" */
    /* eslint block-scoped-var: "off" */
    /* eslint vars-on-top: "off" */
    if (store.getters.getAuth.login) {
      var index = store.getters.getCurrentIndex;
      var name = context.getters.getAccounts[index].name;
    }
    const accounts = context.getters.getAccounts;
    const newAccounts = [...accounts];
    for (let i = 0; i < toPurge.length; i += 1) {
      // if we purge the current account we logout
      if (toPurge[i] === index) {
        adjust = false;
        EventBus.$emit('logout', {
          login: false,
        });
        EventBus.$emit('home');
      }
      newAccounts.splice(toPurge[i], 1);
    }
    context.commit('STORE_ACCOUNTS', newAccounts);
    // we readjust the index to display the name properly
    if (adjust) {
      for (let j = 0; j < newAccounts.length; j += 1) {
        if (name === newAccounts[j].name) {
          store.commit('PUSH_INDEX', j);
          store.commit('STORE_AUTH', {
            login: true,
            index: j,
          });
        }
      }
    }
  },
};

export default {
  state,
  getters,
  actions,
  mutations,
};
