import * as types from '../mutation-types';

// for a an an example you can take a look at
// https://github.com/vuejs/vuex/blob/dev/examples/shopping-cart/store/modules/cart.js
// pp = publicKey

// initial state
const state = {
  passPhrase: '',
  walletName: '',
  address: '',
  PP: '',
  currentIndex: null,
  wallet: null,
};

// getters
const getters = {
  getPassPhrase: state => state.passPhrase,
  getWalletName: state => state.walletName,
  getWallet: state => state.wallet,
  getCurrentIndex: state => state.currentIndex,
  getAddress: state => state.address,
  getPP: state => state.PP,
};

// actions
const actions = {
  pushWallet(context, wallet) {
    context.commit('PUSH_WALLET', wallet);
  },
};

// mutations
const mutations = {
  [types.PUSH_PASSPHRASE](state, passPhrase) {
    state.passPhrase = passPhrase;
  },
  [types.PUSH_WALLETNAME](state, walletName) {
    state.walletName = walletName;
  },
  [types.PUSH_WALLET](state, wallet) {
    state.wallet = wallet;
  },
  [types.PUSH_ADDRESS](state, address) {
    state.address = address;
  },
  [types.PUSH_PP](state, publicKey) {
    state.PP = publicKey;
  },
  [types.PUSH_INDEX](state, index) {
    state.currentIndex = index;
  },
};

export default {
  state,
  getters,
  actions,
  mutations,
};
