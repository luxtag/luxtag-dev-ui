// actions are to push commits to the store state
// they are run synchronously
// import * as types from './mutation-types';

// example for reference (this is temporary)
// export const addToCart = ({ commit }, product) => {
//   if (product.inventory > 0) {
//     commit(types.ADD_TO_CART, {
//       id: product.id
//     });
//   };
// };
