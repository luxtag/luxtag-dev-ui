import * as types from './mutation-types';

const mutations = {
  [types.STORE_AUTH](state, auth) {
    state.auth = auth;
  },
  [types.SET_RESPONSE](state, res) {
    state.response = res;
  },
};
export default mutations;
