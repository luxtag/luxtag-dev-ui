export const getAuth = state => state.auth;

export const getBarsarispp = state => state.barsarispp;

export const getEndpoints = state => state.endpoints;

export const getResponse = state => state.response;
