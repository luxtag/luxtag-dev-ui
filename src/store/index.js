import Vue from 'vue';
import Vuex from 'vuex';
import createPersistedState from 'vuex-persistedstate';
import * as getters from './getters';
import mutations from './mutations';
import * as actions from './actions';
import accounts from './modules/accounts';
import wallet from './modules/wallet';

Vue.use(Vuex);

// this line is to make sure that
// we don't use the debug mod in production
const debug = process.env.NODE_ENV !== 'production';

export default new Vuex.Store({
  state: {
    auth: {
      login: false,
      index: 0,
      PP: '',
    },
    barsarispp: '',
    endpoints: {
      websocket: [{
        protocol: 'http',
        host: 'https://adamexperimental.cyberblox.my',
        port: 7779,
      },
      {
        protocol: 'http',
        host: 'https://pretestnet1.nem.ninja',
        port: 7779,
      }],
      web: [{
        protocol: 'http',
        host: 'https://adamexperimental.cyberblox.my',
        port: 7891,
      },
      {
        protocol: 'http',
        host: 'https://pretestnet1.nem.ninja',
        port: 7891,
      }],
    },
    response: null,
  },
  actions,
  getters,
  mutations,
  modules: {
    accounts,
    wallet,
  },
  strict: debug,
  // https://github.com/robinvdvleuten/vue-auth0-vuex/blob/master/template/src/store/index.js#L13
  plugins: [createPersistedState({
    key: 'LuxTag',
    paths: ['auth', 'accounts.wallets', 'wallet.passPhrase'],
  })],
});
