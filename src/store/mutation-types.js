// here we list all the mutations that we can perform on our store state
// an easy way to come back and check what are the mutations available
// a mutation is a sync operation actions are async

// global
export const STORE_AUTH = 'STORE_AUTH';
export const SET_RESPONSE = 'SET_RESPONSE';
// wallet
export const PUSH_PASSPHRASE = 'PUSH_PASSPHRASE';
export const PUSH_WALLETNAME = 'PUSH_WALLETNAME';
export const PUSH_WALLET = 'PUSH_WALLET';
export const PUSH_ADDRESS = 'PUSH_ADDRESS';
export const PUSH_PP = 'PUSH_PP';
export const PUSH_INDEX = 'PUSH_INDEX';
// accounts in localStorage
export const STORE_ACCOUNT = 'STORE_ACCOUNT';
export const STORE_ACCOUNTS = 'STORE_ACCOUNTS';
