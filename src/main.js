import 'normalize.css';
import Vue from 'vue';
// later for better performance
// it's better to import only the needed
// component modules for element-ui
// http://element.eleme.io/#on-demand
import ElementUI from 'element-ui';
import locale from 'element-ui/lib/locale/lang/en';
import VueQriously from 'vue-qriously';
import App from './App';
import router from './router';
import store from './store';

Vue.config.productionTip = false;

Vue.use(ElementUI, { locale });
Vue.use(VueQriously);
/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  store,
  render: h => h(App),
});
